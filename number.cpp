/*!
*\file
*\brief файл с функцией number()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** number(int** matrix, int  lines, int columns, int number)
/*!
*\brief Возвращает матрицу, умноженную на число.
* Используется в функции menu()
*\param[in] matrix Матрица
*\param[in] lines Количество строк
*\param[in] columns Количество столбцов
*\param[in] number Число, на которое необходимо умножить матрицу
*\return Матрицу, умноженную на число.
*/
{
    int** num = nullptr;
    num = new int* [lines];
    for (int i = 0; i < lines; i++)
    {
        num[i] = new int[columns];
    }

    for (int i = 0; i < lines; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            num[i][j] =  matrix[i][j] * number;
        }
    }

    return num;
}
