Данный проект является программой для работы с матрицами

**Функции**

* Заполнение матрицы
    * Транспонирование матрицы
* Вычитание матриц
* Умножение матриц
* Проверка равенства матриц
* Умножение всех элементов матрицы на число х
* Поиск наименьшего значения элементов матрицы
* Поиск набольшего значения элементов матрицы

<p>© 2019 SFU</p>

