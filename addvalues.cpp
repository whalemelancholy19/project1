/*!
*\file
*\brief файл с функцией addvalues()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>
#include <conio.h>
#include <time.h>
#include "functions.h"

using namespace std;

int** addvalues(int  lines, int columns)
/*!
*\brief Функция возвращает заполненную матрицу.
* Используется в функции menu()
*\param[in] lines Количество строк
*\param[in] columns Количество столбцов
*\return Заполненную матрицу.
*/
{
    int** matrix = new int* [lines];
    for (int i = 0; i < lines; i++)
    {
        matrix[i] = new int[columns];
    }

    for (int i = 0; i < lines; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            matrix[i][j] = rand() %20;
        }
    }

    return matrix;
}
