/*!
*\file
*\brief файл с функцией summ()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** summ(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b)
/*!
*\brief Функция возвращает матрицу суммы.
* Используется в функции menu()
*\param[in] matrix_a Первая матрица
*\param[in] lines_a Количество строк первой матрицы
*\param[in] columns_a Количество столбцов первой матрицы
*\param[in] matrix_b Вторая матрица
*\param[in] lines_b Количество строк второй матрицы
*\param[in] columns_b Количество столбцов второй матрицы
*\return Матрицу суммы.
*/
{
    int** sum = nullptr;

    sum = new int* [lines_a];
    for (int i = 0; i < lines_a; i++)
    {
        sum[i] = new int[columns_a];
    }

    for (int i = 0; i < lines_a; i++)
    {
        for (int j = 0; j < columns_a; j++)
        {
            sum[i][j] = matrix_a[i][j] + matrix_b[i][j];
        }
    }


    return sum;
}
