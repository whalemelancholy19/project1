/*!
*\file
*\brief файл с функцией equality()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

bool equality(int** matrix_a, int lines_a, int columns_a, int** matrix_b, int lines_b, int columns_b)
/*!
*\brief Функция сравнивает две матрицы.
* Используется в функции menu()
*\param[in] matrix_a Первая матрица
*\param[in] lines_a Количество строк первой матрицы
*\param[in] columns_a Количество столбцов первой матрицы
*\param[in] matrix_b Вторая матрица
*\param[in] lines_b Количество строк второй матрицы
*\param[in] columns_b Количество столбцов второй матрицы
*\return true, если матрицы равны и false, если не равны.
*/
{
     bool check = true;

     if ((lines_a != lines_b) || (columns_a != columns_b))
     {
         return false;
     }
     else
     {
        for (int j = 0; j < columns_a; j++)
        {
            for (int i = 0; i < lines_a; i++)
            {
                if (matrix_a[i][j] != matrix_b[i][j])
                {
                    check = false;
                }
            }
        }
     }

     return check;
}
