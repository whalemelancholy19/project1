/*!
*\file
*\brief файл с функцией sudtraction()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** sudtraction(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b, int choice5)
/*!
*\brief Функция возвращает матрицу разности.
* Используется в функции menu()
*\param[in] matrix_a Первая матрица
*\param[in] lines_a Количество строк первой матрицы
*\param[in] columns_a Количество столбцов первой матрицы
*\param[in] matrix_b Вторая матрица
*\param[in] lines_b Количество строк второй матрицы
*\param[in] columns_b Количество столбцов второй матрицы
*\param[in] choice5 Выбор, из какой матрицы какую вычитать
*\return Матрицу разности.
*/
{
    int** sud = nullptr;
    sud = new int* [lines_a];
    for (int i = 0; i < lines_a; i++)
    {
        sud[i] = new int[columns_a];
    }

    if (choice5 == 1)
    {
        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_a; j++)
            {
                sud[i][j] = matrix_a[i][j] - matrix_b[i][j];
            }
        }
    }
    else if (choice5 == 2)
    {
        for (int i = 0; i < lines_a; i++)
        {
            for (int j = 0; j < columns_a; j++)
            {
                sud[i][j] = matrix_b[i][j] - matrix_a[i][j];
            }
        }
    }

    return  sud;
}
