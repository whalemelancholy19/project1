/*!
*\file
*\brief файл с функцией findMin()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

/*!
*\brief Находит наименьшее значение среди элементов матрицы.
* Используется в функции menu()
*\param[in] matrix Матрица
*\param[in] lines Количество строк
*\param[in] columns Количество столбцов
*\return Минимальное значение.
*/
int findMin(int** matrix, int  lines, int columns)
{
    int min = matrix[0][0];

    for (int i = 0; i < lines; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            if (matrix[i][j] < min)
                min  = matrix[i][j];
        }
    }

    return min;
}
