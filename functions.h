#ifndef FUNCTIONS_H
#define FUNCTIONS_H

void printMatrix(int** matrix, int lines, int columns);
void menu();
int** addvalues(int  lines, int columns);
bool equality(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b);
int** transposed(int** matrix, int  lines, int columns);
int** summ(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b);
int** sudtraction(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b, int choice5);
int** composit(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b);
int** number(int** matrix, int  lines, int columns, int number);
int findMin(int** matrix, int  lines, int columns);
int findMax(int** matrix, int  lines, int columns);

#endif // FUNCTIONS_H
