/*!
*\file
*\brief файл с функцией transposed()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** transposed(int** matrix, int  lines, int columns)
/*!
*\brief Функция возвращает транспонированную матрицу.
* Используется в функции menu()
*\param[in] lines Количество строк
*\param[in] columns Количество столбцов
*\return Заполненную транспонированную матрицу.
*/
{
    int** trans = nullptr;
    trans = new int* [columns];
    for (int i = 0; i < columns; i++)
    {
        trans[i] = new int[lines];
    }

    for (int j = 0; j < columns; j++)
    {
        for (int i = 0; i < lines; i++)
        {
            trans[j][i] = matrix[i][j];
        }
    }

    return trans;
}
