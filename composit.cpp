/*!
*\file
*\brief файл с функцией composit()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int** composit(int** matrix_a, int  lines_a, int columns_a, int** matrix_b, int  lines_b, int columns_b)
/*!
*\brief Функция возвращает матрицу произведения.
* Используется в функции menu()
*\param[in] matrix_a Первая матрица
*\param[in] lines_a Количество строк первой матрицы
*\param[in] columns_a Количество столбцов первой матрицы
*\param[in] matrix_b Вторая матрица
*\param[in] lines_b Количество строк второй матрицы
*\param[in] columns_b Количество столбцов второй матрицы
*\return Заполненную матрицу.
*/
{
    int** com = nullptr;
    com = new int* [lines_a];
    for (int i = 0; i < lines_a; i++)
    {
        com[i] = new int[columns_b];
        for (int j = 0; j<columns_b; j++)
        {
            com[i][j] = 0;
            for (int k = 0; k < columns_a; k++)
            {
                com[i][j] += matrix_a[i][k] * matrix_b[k][j];
            }
        }
    }

    return com;
}
