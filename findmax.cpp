/*!
*\file
*\brief файл с функцией findMax()
*/
#include "functions.h"
#include <iostream>
#include <QDebug>

using namespace std;

int findMax(int** matrix, int  lines, int columns)
/*!
*\brief Находит наибольшее значение среди элементов матрицы.
* Используется в функции menu()
*\param[in] matrix Матрица
*\param[in] lines Количество строк
*\param[in] columns Количество столбцов
*\return Максимальное значение.
*/
{
    int max = matrix[0][0];

    for (int i = 0; i < lines; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            if (matrix[i][j] > max)
                max  = matrix[i][j];
        }
    }

    return max;
}
