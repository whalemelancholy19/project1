#include "functions.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <time.h>
#include <QVector>
#include <QDebug>
using namespace std;

void printMatrix(int** matrix, int lines, int columns)
{
    for (int i = 0; i < lines; i++)
    {
        for (int j = 0; j < columns; j++)
        {
            cout << matrix[i][j] << "\t";
        }
        cout << "\n";
    }
}

void menu()
{
    int **matrix_a = nullptr;
    int  lines_a = 0;
    int columns_a = 0;
    bool inputA = false;

    int **matrix_b = nullptr;
    int  lines_b = 0;
    int columns_b = 0;
    bool inputB = false;

    int **sum = nullptr;
    int **com = nullptr;
    int **sud = nullptr;
    int **num = nullptr;
    int **trans = nullptr;

    int min = 0;
    int max = 0;

    int choice = 0;
    int choice2 = 0;
    int numbern = 0;

    bool check = true;


    do
    {
        system("cls");
        cout << "--------------------------------------------------\n";
        cout << "1. ���������� ������� �\n";
        cout << "2. ���������� ������� �\n";
        cout << "3. ���������������� �������\n";
        cout << "4. �������� ������ � � �\n";
        cout << "5. ��������� ������ � � �\n";
        cout << "6. ��������� ������ � � �\n";
        cout << "7. �������� ��������� ������ � � �\n";
        cout << "8. ��������� ���� ��������� ������� �� ����� �\n";
        cout << "9. ����� ����������� �������� ��������� �������\n";
        cout << "10. ����� ���������� �������� ��������� �������\n";
        cout << "--------------------------------------------------\n";
        cout << "\n\n";

        do
        {
            printf("�������� ����� ����: ");
            char c = '\0';
            while (scanf("%d%c", &choice, &c, 1) != 2 || c != '\n')
            {
                printf("�������� ����� ����: ");
                while (getchar() != '\n');
            }
        } while ((choice < 1) || (choice > 10));

        if (choice == 11)
            break;

        switch (choice)
        {
        case 1:
            system("cls");
            if (inputA == true)
            {
                for (int i = 0; i < lines_a; i++)
                {
                    delete[] matrix_a[i];
                }
                delete[] matrix_a;
            }

            do
            {
                cout << "������� ���������� ����� � ������� �: ";
                char c = '\0';
                while (scanf("%d%c", &lines_a, &c, 1) != 2 || c != '\n')
                {
                    cout << "������� ���������� ����� � ������� �: ";
                    while (getchar() != '\n');
                }
            } while ((lines_a < 1) || (lines_a > 9));


            do
            {
                cout << "������� ���������� �������� � ������� A: ";
                char c = '\0';
                while (scanf("%d%c", &columns_a, &c, 1) != 2 || c != '\n')
                {
                    cout << "������� ���������� �������� � ������� A: ";
                    while (getchar() != '\n');
                }
            } while ((columns_a < 1) || (columns_a > 9));

            matrix_a = addvalues(lines_a, columns_a);

            cout << "\n\n";
            cout << "M������ A:\n";
            printMatrix(matrix_a, lines_a, columns_a);
            inputA = true;
            system("pause");
            system("cls");
            break;
        case 2:
            system("cls");
            if (inputB == true)
            {
                for (int i = 0; i < lines_b; i++)
                {
                    delete[] matrix_b[i];
                }
                delete[] matrix_b;
            }

            do
            {
                cout << "������� ���������� ����� � ������� B: ";
                char c = '\0';
                while (scanf("%d%c", &lines_b, &c, 1) != 2 || c != '\n')
                {
                    cout << "������� ���������� ����� � ������� B: ";
                    while (getchar() != '\n');
                }
            } while ((lines_b < 1) || (lines_b > 9));

            do
            {
                cout << "������� ���������� �������� � ������� �: ";
                char c = '\0';
                while (scanf("%d%c", &columns_b, &c, 1) != 2 || c != '\n')
                {
                    cout << "������� ���������� �������� � ������� �: ";
                    while (getchar() != '\n');
                }
            } while ((columns_b < 1) || (columns_b > 9));

            matrix_b =  addvalues(lines_b, columns_b);

            cout << "\n\n";
            cout << "M������ B:\n";
            printMatrix(matrix_b, lines_b, columns_b);
            inputB = true;
            system("pause");
            system("cls");
            break;
        case 3:
            system("cls");
            cout << "1. ��������������� ������� �\n";
            cout << "2. ��������������� ������� �\n";
            do
            {
                cout << "�������� ����� ����: ";
                char c = '\0';
                while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                {
                    cout << "�������� ����� ����: ";
                    while (getchar() != '\n');
                }
            } while ((choice2 < 1) || (choice2 > 2));
            if (choice2 == 1)
            {
                system("cls");
                if (inputA == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout << "�������� �������:\n";
                    printMatrix(matrix_a, lines_a, columns_a);

                    trans = transposed(matrix_a, lines_a, columns_a);

                    cout << "\n\n\n";
                    cout << "����������������� �������:\n";
                    printMatrix(trans, columns_a, lines_a);

                    for (int i = 0; i < columns_a; i++)
                    {
                        delete[] trans[i];
                    }
                    delete[] trans;
                }
            }
            else
            {
                system("cls");
                if (inputB == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout << "�������� �������:\n";
                    printMatrix(matrix_b, lines_b, columns_b);

                    trans = transposed(matrix_b, lines_b, columns_b);

                    cout << "\n\n\n";
                    cout << "����������������� �������:\n";
                    printMatrix(trans, columns_b, lines_b);

                    for (int i = 0; i < columns_b; i++)
                    {
                        delete[] trans[i];
                    }
                    delete[] trans;
                }
            }
            system("pause");
            system("cls");
            break;
        case 4:
            system("cls");
            if ((inputA == false) || (inputB == false))
            {
                cout << "����������, ������� ��������� �������\n";
            }
            else
            {
                if ( lines_a != lines_b || columns_a != columns_b)
                {
                    cout << "�������� ����������, �.�. ������ ������ �� ���������!\n";
                }
                else
                {
                    cout <<"�������� ������� A: \n";
                    printMatrix(matrix_a, lines_a, columns_a);

                    cout <<"�������� ������� B: \n";
                    printMatrix(matrix_b, lines_b, columns_b);

                    sum = summ(matrix_a, lines_a, columns_a, matrix_b, lines_b, columns_b);
                    cout << "\n\n\n";
                    cout <<"������� �����: \n";
                    printMatrix(sum, lines_a, columns_a);


                    if ( lines_a == lines_b && columns_a == columns_b)
                    {
                        for (int i = 0; i < lines_a; i++)
                        {
                            delete[] sum[i];
                        }
                        delete[] sum;
                    }
                }

            }
            system("pause");
            system("cls");
            break;
        case 5:
            system("cls");
            if ((inputA == false) || (inputB == false))
            {
                cout << "����������, ������� ��������� �������\n";
            }
            else
            {
                if ((lines_a != lines_b) || (columns_a != columns_b))
                {
                    cout << "���������� ������� ������� ������ ������������\n";
                }
                else
                {
                    cout << "1. A-B\n";
                    cout << "2. B-A\n";

                    do
                    {
                        cout << "�������� ����� ����: ";
                        char c = '\0';
                        while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                        {
                            cout << "�������� ����� ����: ";
                            while (getchar() != '\n');
                        }
                    } while ((choice2 < 1) || (choice2 > 2));
                    system("cls");

                    cout << "�������� ������� A:\n";
                    printMatrix(matrix_a, lines_a, columns_a);

                    cout << "�������� ������� B:\n";
                    printMatrix(matrix_b, lines_b, columns_b);
                    cout << "\n\n\n";

                    if (choice2 == 1)
                    {
                        sud = sudtraction(matrix_a, lines_a,  columns_a,  matrix_b, lines_b, columns_b, choice2);
                        cout << "������� A-B:\n";
                        printMatrix(sud, lines_a, columns_a);
                    }
                    else if (choice2 == 2)
                    {
                        sud = sudtraction(matrix_a, lines_a,  columns_a,  matrix_b, lines_b, columns_b, choice2);
                        cout << "������� B-A:\n";
                        printMatrix(sud, lines_b, columns_b);
                    }

                    if ( lines_a == lines_b && columns_a == columns_b)
                    {
                        for (int i = 0; i < lines_a; i++)
                        {
                            delete[] sud[i];
                        }
                        delete[] sud;
                    }
                }

            }
            system("pause");
            system("cls");
            break;
        case 6:
            system("cls");
            if ((inputA == false) || (inputB == false))
            {
                cout << "����������, ������� ��������� �������\n";
            }
            else
            {
                if(columns_a != lines_b)
                {
                    cout << "��� ��������� ����������, ����� ���������� �������� \n ������ ������� ���� ����� ���������� ����� ������\n";
                }
                else
                {
                    cout << "�������� ������� A: \n";
                    printMatrix(matrix_a, lines_a, columns_a);

                    cout <<"�������� ������� B: \n";
                    printMatrix(matrix_b, lines_b, columns_b);

                    com = composit(matrix_a,  lines_a, columns_a, matrix_b, lines_b, columns_b);

                    cout << "\n\n\n";
                    cout<< "������� ������������: \n";
                    printMatrix(com, lines_a, columns_b);


                    if(columns_a == lines_b)
                    {
                        for (int i = 0; i < lines_a; i++)
                        {
                            delete[] com[i];
                        }
                        delete[] com;
                    }
                }


            }
            system("pause");
            system("cls");
            break;
        case 7:
            system("cls");
            if ((inputA == false) || (inputB == false))
            {
                cout << "����������, ������� ��������� �������\n";
            }
            else
            {
                check = equality(matrix_a, lines_a, columns_a, matrix_b, lines_b, columns_b);
                if (check == true)
                {
                    cout << "������� �����\n";
                }
                else
                {
                    cout << "������� �� �����\n";
                }
            }
            system("pause");
            system("cls");
            break;
        case 8:
            system("cls");
            cout << "1. �������� �� ����� ������� �\n";
            cout << "2. �������� �� ����� ������� �\n";
            do
            {
                cout << "�������� ����� ����: ";
                char c = '\0';
                while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                {
                    cout << "�������� ����� ����: ";
                    while (getchar() != '\n');
                }
            } while ((choice2 < 1) || (choice2 > 2));
            system("cls");
            if (choice2 == 1)
            {
                system("cls");
                if (inputA == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    do
                    {
                        cout << "�� ����� ����� ����� �������� �������?\n";
                        char c = '\0';
                        while (scanf("%d%c", &numbern, &c, 1) != 2 || c != '\n')
                        {
                            cout << "�������� ��������. ����������, ���������� �����.\n";
                            while (getchar() != '\n');
                        }
                    } while ((numbern < -100) || (numbern > 100));

                    cout << "\n\n\n";
                    cout <<"�������� �������: \n";
                    printMatrix(matrix_a, lines_a, columns_a);
                    cout << "\n\n\n";

                    num = number(matrix_a, lines_a, columns_a, numbern);

                    cout <<"�������, ���������� �� "<< numbern << ": \n";
                    printMatrix(num, lines_a, columns_a);

                    for (int i = 0; i < lines_a; i++)
                    {
                        delete[] num[i];
                    }
                    delete[] num;
                }
            }
            else
            {
                system("cls");
                if (inputB == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    do
                    {
                        cout << "�� ����� ����� ����� �������� �������?\n";
                        char c = '\0';
                        while (scanf("%d%c", &numbern, &c, 1) != 2 || c != '\n')
                        {
                            cout << "�������� ��������. ����������, ���������� �����.\n";
                            while (getchar() != '\n');
                        }
                    } while ((numbern < -100) || (numbern > 100));
                    cout << "\n\n\n";
                    cout <<"�������� �������: \n";
                    printMatrix(matrix_b, lines_b, columns_b);
                    cout << "\n\n\n";

                    num = number(matrix_b, lines_b, columns_b, numbern);

                    cout <<"�������, ���������� �� "<< numbern << ": \n";
                    printMatrix(num, lines_b, columns_b);
                    for (int i = 0; i < lines_b; i++)
                    {
                        delete[] num[i];
                    }
                    delete[] num;
                }
            }
            system("pause");
            system("cls");
            break;
        case 9:
            system("cls");
            cout << "1. ����� ����������� �������� ��������� ������� �\n";
            cout << "2. ����� ����������� �������� ��������� ������� �\n";
            do
            {
                cout << "�������� ����� ����: ";
                char c = '\0';
                while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                {
                    cout << "�������� ����� ����: ";
                    while (getchar() != '\n');
                }
            } while ((choice2 < 1) || (choice2 > 2));
            system("cls");
            if (choice2 == 1)
            {
                system("cls");
                if (inputA == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout <<"�������� �������: \n";
                    printMatrix(matrix_a, lines_a, columns_a);
                    cout << "\n\n";

                    min = findMin(matrix_a, lines_a, columns_a);

                    cout << "���������� �������� ��������� ������� � ����� " << min;

                }
            }
            else
            {
                system("cls");
                if (inputB == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout <<"�������� �������: \n";
                    printMatrix(matrix_b, lines_b, columns_b);
                    cout << "\n\n";

                    min = findMin(matrix_b, lines_b, columns_b);

                    cout << "���������� �������� ��������� ������� B ����� " << min;
                }
            }
            cout << "\n\n";
            system("pause");
            system("cls");
            break;
        case 10:
            system("cls");
            cout << "1. ����� ����������� �������� ��������� ������� �\n";
            cout << "2. ����� ����������� �������� ��������� ������� �\n";
            do
            {
                cout << "�������� ����� ����: ";
                char c = '\0';
                while (scanf("%d%c", &choice2, &c, 1) != 2 || c != '\n')
                {
                    cout << "�������� ����� ����: ";
                    while (getchar() != '\n');
                }
            } while ((choice2 < 1) || (choice2 > 2));
            system("cls");
            if (choice2 == 1)
            {
                system("cls");
                if (inputA == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout <<"�������� �������: \n";
                    printMatrix(matrix_a, lines_a, columns_a);
                    cout << "\n\n";

                    max = findMax(matrix_a, lines_a, columns_a);

                    cout << "���������� �������� ��������� ������� � ����� " << max;

                }
            }
            else
            {
                system("cls");
                if (inputB == false)
                {
                    cout << "����������, ������� ��������� �������\n";
                }
                else
                {
                    cout <<"�������� �������: \n";
                    printMatrix(matrix_b, lines_b, columns_b);
                    cout << "\n\n";

                    max = findMax(matrix_b, lines_b, columns_b);

                    cout << "���������� �������� ��������� ������� B ����� " << max;
                }
            }
            cout << "\n\n";
            system("pause");
            system("cls");
            break;
//        case 11:
//            if (inputA == true)
//            {
//                for (int i = 0; i < lines_a; i++)
//                {
//                    delete[] matrix_a[i];
//                }
//                delete[] matrix_a;
//            }

//            if (inputB == true)
//            {
//                for (int i = 0; i < lines_b; i++)
//                {
//                    delete[] matrix_b[i];
//                }
//                delete[] matrix_b;
//            }
//            break;
        }

    } while(choice != 12);

}
